#include "PushdownState.h"

using namespace NCL::CSC8503;

PushdownState::~PushdownState()
{

}

PushdownState::PushdownResult PushdownState::PushdownUpdate(PushdownState** pushResult) {

	return PushdownResult::NoChange;
}

void PushdownState::Update(float dt)
{
	updateDelegate(this, dt);
}