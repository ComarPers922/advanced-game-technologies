#include "PushdownMachine.h"
#include "PushdownState.h"
using namespace NCL::CSC8503;

PushdownMachine::PushdownMachine()
{
	// activeState = nullptr;
}

PushdownMachine::~PushdownMachine()
{
}

void PushdownMachine::Update(float dt) 
{
	//if (activeState) {
	//	PushdownState* newState = nullptr;
	//	PushdownState::PushdownResult result = activeState->PushdownUpdate(&newState);

	//	switch (result) {
	//		case PushdownState::Pop: {
	//			activeState->OnSleep();
	//			stateStack.pop();
	//			if (stateStack.empty()) {
	//				activeState = nullptr; //??????
	//			}
	//			else {
	//				activeState = stateStack.top();
	//				activeState->OnAwake();
	//			}
	//		}break;
	//		case PushdownState::Push: {
	//			activeState->OnSleep();
	//			stateStack.push(newState);
	//			newState->OnAwake();
	//		}break;
	//	}

	/*}*/
	if (!stateStack.empty())
	{
		stateStack.top()->Update(dt);
	}
}

void NCL::CSC8503::PushdownMachine::PushState(PushdownState* newState)
{
	if (!stateStack.empty())
	{
		stateStack.top()->OnSleep();
	}
	newState->OnAwake();
	stateStack.push(newState);
}

PushdownState* NCL::CSC8503::PushdownMachine::PopState()
{
	stateStack.top()->OnSleep();
	auto* result = stateStack.top();
	stateStack.pop();
	if (!stateStack.empty())
	{
		stateStack.top()->OnAwake();
	}
	return result;
}
