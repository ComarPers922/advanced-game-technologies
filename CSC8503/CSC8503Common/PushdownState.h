#pragma once
#include "State.h"

#include <functional>

namespace NCL {
	namespace CSC8503 {
		class PushdownState :
			public State
		{
		public:
			enum PushdownResult 
			{
				Push, Pop, NoChange
			};
			PushdownState(std::function<void(PushdownState*, float)> updateDelegate = [](PushdownState* sender, float dt) {},
				std::function<void(PushdownState*)> awakeDelegate = [](PushdownState* sender) {},
				std::function<void(PushdownState*)> sleepDelegate = [](PushdownState* sender) {}):
				updateDelegate(updateDelegate),
				awakeDelegate(awakeDelegate),
				sleepDelegate(sleepDelegate)
			{

			}
			~PushdownState();

			PushdownResult PushdownUpdate(PushdownState** pushResult);

			virtual void OnAwake()
			{
				awakeDelegate(this);
			} 
			virtual void OnSleep()
			{
				sleepDelegate(this);
			}
			virtual void Update(float dt);
			virtual void Update()
			{
				// Do nothing!!!
			}
		private:
			const std::function<void(PushdownState*, float)> updateDelegate;
			const std::function<void(PushdownState*)> sleepDelegate;
			const std::function<void(PushdownState*)> awakeDelegate;
		};
	}
}

