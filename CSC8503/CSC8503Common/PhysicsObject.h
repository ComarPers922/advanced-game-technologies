#pragma once
#include "../../Common/Vector3.h"
#include "../../Common/Matrix3.h"

#include <unordered_map>
#include <unordered_set>

using namespace NCL::Maths;
using std::unordered_map;
using std::unordered_set;

#define COLLISION_TABLE unordered_map<Layer, unordered_set<Layer>>

namespace NCL
{
	class CollisionVolume;
	namespace CSC8503
	{
		enum PhysicsType
		{
			None, Static, Dynamic, MAX_PHYSICS_TYPE
		};
		enum Layer
		{
			Default, Wall, Player, Floor, Apple, Bonus, Enemy, Water, SpawnPoint, MAX_LAYER
		};

		const COLLISION_TABLE COLLISION_MAP =
			COLLISION_TABLE
			({
				{Layer::Default, {Default, Wall, Player, Floor, Apple, Bonus, Enemy, Water, SpawnPoint}},
				{Layer::Wall, {Default, Player/*, Enemy*/}},
				{Layer::Player, {Default,Wall, Player, Floor, Enemy, Apple, Bonus, Water, SpawnPoint}},
				{Layer::Floor, {Default, Player, Enemy, Apple}},
				{Layer::Apple, {Default, Player, Floor, Apple}},
				{Layer::Bonus, {Default, Player}},
				{Layer::Enemy, {Default, Player, /*Wall,*/ Floor, Water, Enemy}},
				{Layer::Water, {Default, Player, Enemy}},
				{Layer::SpawnPoint, {Player}}
				});

		class Transform;

		class PhysicsObject	{
		public:
			PhysicsObject(Transform* parentTransform, 
				const CollisionVolume* parentVolume, 
				const PhysicsType& type = PhysicsType::None, 
				const Layer& layer = Layer::Default,
				const bool& isTrigger = false);
			~PhysicsObject();

			Vector3 GetLinearVelocity() const {
				return linearVelocity;
			}

			Vector3 GetAngularVelocity() const {
				return angularVelocity;
			}

			Vector3 GetTorque() const {
				return torque;
			}

			Vector3 GetForce() const {
				return force;
			}

			void SetInverseMass(float invMass) {
				inverseMass = invMass;
			}

			float GetInverseMass() const {
				return inverseMass;
			}

			void SetPhysicsLayer(Layer val)
			{
				physicsLayer = val;
			}

			Layer GetPhysicsLayer()
			{
				return physicsLayer;
			}

			void ApplyAngularImpulse(const Vector3& force);
			void ApplyLinearImpulse(const Vector3& force);
			
			void AddForce(const Vector3& force);

			void AddForceAtPosition(const Vector3& force, const Vector3& position);

			void AddTorque(const Vector3& torque);


			void ClearForces();

			void SetLinearVelocity(const Vector3& v) {
				linearVelocity = v;
			}

			void SetAngularVelocity(const Vector3& v) {
				angularVelocity = v;
			}

			void InitCubeInertia();
			void InitSphereInertia();

			void UpdateInertiaTensor();

			Matrix3 GetInertiaTensor() const {
				return inverseInteriaTensor;
			}
			void SetElasticity(float val)
			{
				elasticity = val;
			}
			float GetElasticity()
			{
				return elasticity;
			}

			PhysicsType GetPhysicsType()
			{
				return physicsType;
			}

			bool GetIsTriggered()
			{
				return isTrigger;
			}

		protected:
			const CollisionVolume* volume;
			Transform*		transform;

			float inverseMass;
			float elasticity;
			float friction;

			//linear stuff
			Vector3 linearVelocity;
			Vector3 force;
			

			//angular stuff
			Vector3 angularVelocity;
			Vector3 torque;
			Vector3 inverseInertia;
			Matrix3 inverseInteriaTensor;

			Layer physicsLayer;

			const PhysicsType physicsType;
			const bool isTrigger;
		};
	}
}

