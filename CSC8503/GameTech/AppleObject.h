#pragma once
#include <iostream>
#include <functional>

#include "../CSC8503Common/GameObject.h"
#include "../CSC8503Common/StateMachine.h"
#include "../CSC8503Common/StateTransition.h"
#include "../CSC8503Common/State.h"
#include "../../Common/Vector3.h"
#include "DestinationObject.h"

namespace NCL
{
	namespace CSC8503
	{
		class AppleObject : public GameObject
		{
		public:
			const int Points = 5;
			AppleObject(GameObject* player, 
				Vector3 origin, 
				DestinationObject* destObj, 
				int points,
				std::function<void(AppleObject*)> pickedAppleDelegate = [](AppleObject* sender) {});
			~AppleObject();

			virtual void OnCollisionBegin(GameObject* otherObject);

			GameObject* GetPlayerObject()
			{
				return playerObject;
			}

			void UpdateStateMachine();

			void LoseApple()
			{
				isAttatchedToPlayer = false;
			}
			void IsAtDestination()
			{
				offsetYDest = destObject->GetNumberOfApples();
				isAtDest = true;
			}

			void Reset()
			{
				isReset = true;
				isAttatchedToPlayer = false;
				isAtDest = false;
			}
		private:
			const std::function<void(AppleObject*)> pickedAppleDelegate;

			GameObject* playerObject;
			DestinationObject* destObject;
			const Vector3 originalPoint;

			StateMachine* appleStateMachine;

			const StateFunc idleFunc;
			const StateFunc pickedFunc;
			const StateFunc destFunc;

			GenericState* idleState;
			GenericState* pickedState;
			GenericState* destState;

			GenericTransition<bool&, bool>* idleToPicked;
			GenericTransition<bool&, bool>* pickedToIdle;
			GenericTransition<bool&, bool>* pickedToDest;
			GenericTransition<bool&, bool>* destToIdle;

			bool isAttatchedToPlayer = false;
			bool isAtDest = false;
			bool isReset = false;

			float offsetYGoose = 0;
			float offsetYDest = 0;
		};
	}
}
