#pragma once
#include <deque>
#include <cstdlib>
#include <ctime>

template<class T>
class RandomQueue
{
private:
	std::deque<T> queue;
public:
    RandomQueue()
    {
        srand(time(NULL));
    }

    void add(T data)
    {
        if(rand() % 100 < 50)
        {
            queue.push_back(data);
        }
        else
        {
            queue.push_front(data);
        }
    }

    T remove()
    {
        if(rand() % 100 < 50)
        {
            T data = queue.front();
            queue.pop_front();
            return data;
        }
        else
        {
            T data = queue.back();
            queue.pop_back();
            return data;
        }
    }

    int size()
    {
      return queue.size();
    }

    bool isEmpty()
    {
        return size() == 0;
    }
};

