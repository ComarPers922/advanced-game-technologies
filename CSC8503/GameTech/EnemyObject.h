#pragma once
#include "..\CSC8503Common\GameObject.h"
#include "..\CSC8503Common\StateMachine.h"
#include "..\CSC8503Common\StateTransition.h"
#include "..\CSC8503Common\State.h"

#include "GoosePlayer.h"
#include "mazedata.h"
#include "../CSC8503Common/Debug.h"

#include<vector>

namespace NCL
{
	namespace CSC8503
	{
		class EnemyObject :
			public GameObject
		{
		public:
			EnemyObject(GoosePlayer* player, MazeData* map, Vector3 initPos);
			~EnemyObject();

			virtual void OnCollisionBegin(GameObject* otherObject)
			{
				if (otherObject->GetPhysicsObject()->GetPhysicsLayer() == Layer::Player)
				{

				}
				if (otherObject->GetPhysicsObject()->GetPhysicsLayer() == Layer::Water)
				{
					isInWater = true;
				}
			}

			virtual void OnCollisionEnd(GameObject* otherObject)
			{
				if (otherObject->GetPhysicsObject()->GetPhysicsLayer() == Layer::Water)
				{
					isInWater = false;
				}
			}

			void UpdateStateMachine()
			{
				float distance = (transform.GetWorldPosition() - playerObject->GetTransform().GetWorldPosition()).Length();
				shouldChase = (playerObject != nullptr && 
					playerObject->GetNumberOfPickedApples() > 0 && 
					distance <= 25);
				enemyStateMachine->Update();
			}

			std::vector<Vector3>& GetChasingPath()
			{
				return chasingPath;
			}

			bool IsInWater()
			{
				return isInWater;
			}			
			const Vector3 initPos;
		private:
			bool isInWater = false;

			GoosePlayer* playerObject;

			StateMachine* enemyStateMachine;

			const StateFunc idleFunc;
			const StateFunc chasingFunc;

			GenericState* idleState;
			GenericState* chasingState;

			GenericTransition<bool&, bool>* idleToChasing;
			GenericTransition<bool&, bool>* chasingToIdle;

			MazeData* gameMap;

			bool shouldChase = false;

			std::vector<Vector3> chasingPath;


		};
	}
}
