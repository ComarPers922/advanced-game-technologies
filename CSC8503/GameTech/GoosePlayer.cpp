#include "GoosePlayer.h"

NCL::CSC8503::GoosePlayer::GoosePlayer(Vector3 initPos, std::function<void(GoosePlayer*)> playerDelegate):
	gooseStateMachine(new StateMachine()),
	playerDelegate(playerDelegate),
	initPos(initPos)
{
}

NCL::CSC8503::GoosePlayer::~GoosePlayer()
{
	delete gooseStateMachine;
}

void NCL::CSC8503::GoosePlayer::UpdateStateMachine()
{
	gooseStateMachine->Update();
}
