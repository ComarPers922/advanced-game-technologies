#include "../../Common/Window.h"

#include "../CSC8503Common/StateMachine.h"
#include "../CSC8503Common/StateTransition.h"
#include "../CSC8503Common/State.h"

#include "../CSC8503Common/PushdownMachine.h"
#include "../CSC8503Common/PushdownState.h"

#include "../CSC8503Common/GameServer.h"
#include "../CSC8503Common/GameClient.h"

#include "../CSC8503Common/NavigationGrid.h"

#include "TutorialGame.h"
#include "NetworkedGame.h"
#include "mazedata.h"

using namespace NCL;
using namespace CSC8503;

void TestStateMachine() 
{
	StateMachine* testMachine = new StateMachine();

	int someData = 0;

	StateFunc AFunc = [](void* data)
	{
		int* realData = (int*)data;
		(*realData)++;
		std::cout << "In State A!" << std::endl;
	};
	StateFunc BFunc = [](void* data)
	{
		int* realData = (int*)data;
		(*realData)--;
		std::cout << "In State B!" << std::endl;
	};

	GenericState* stateA = new GenericState(AFunc, (void*)&someData);
	GenericState* stateB = new GenericState(BFunc, (void*)&someData);
	testMachine->AddState(stateA);
	testMachine->AddState(stateB);

	GenericTransition<int&, int>* transitionA = new GenericTransition<int&, int>(GenericTransition<int&, int>::EqualsTransition, someData, 10, stateA, stateB);
	GenericTransition<int&, int>* transitionB = new GenericTransition<int&, int>(GenericTransition<int&, int>::EqualsTransition, someData, 0, stateB, stateA);
	
	testMachine->AddTransition(transitionA);
	testMachine->AddTransition(transitionB);
	for (int i = 0; i < 100; i++)
	{
		testMachine->Update();
	}
	delete testMachine;
	delete stateA;
	delete stateB;
	delete transitionA;
	delete transitionB;

	StateMachine* demoMachine = new StateMachine();
	int num = 100;
	// std::cin >> num;
	bool isGood = (num >= 10);
	StateFunc demoAFunc = [](void* data) 
	{
		std::cout << "I am good!" << std::endl;
	};
	StateFunc demoBFunc = [](void* data)
	{
		std::cout << "I am bad!" << std::endl;
	};
	GenericState* demoStateA = new GenericState(demoAFunc, (void*)&isGood);
	GenericState* demoStateB = new GenericState(demoBFunc, (void*)&isGood);

	demoMachine->AddState(demoStateA);
	demoMachine->AddState(demoStateB);

	GenericTransition<bool&, bool>* demoATransition = new GenericTransition<bool&, bool>(GenericTransition<bool&, bool>::EqualsTransition, isGood, true, demoStateA, demoStateB);
	GenericTransition<bool&, bool>* demoBTransition = new GenericTransition<bool&, bool>(GenericTransition<bool&, bool>::EqualsTransition, isGood, false, demoStateB, demoStateA);

	demoMachine->AddTransition(demoATransition);
	demoMachine->AddTransition(demoBTransition);

	demoMachine->Update();
	delete demoMachine;
	delete demoStateA;
	delete demoStateB;
	delete demoATransition;
	delete demoBTransition;
}

void TestPushdown()
{
	PushdownMachine* machine = new PushdownMachine();
	PushdownState* state0 = new PushdownState([](PushdownState* sender, float dt)
		{
			std::cout << "State 0" << std::endl;
		},
		[](PushdownState* sender)
		{
			std::cout << "State 0 awakes" << std::endl;
		},
		[](PushdownState* sender)
		{
			std::cout << "State 0 sleeps" << std::endl;
		});
	PushdownState* state1 = new PushdownState([](PushdownState* sender, float dt)
		{
			std::cout << "State 1" << std::endl;
		},
		[](PushdownState* sender)
		{
			std::cout << "State 1 awakes" << std::endl;
		},
			[](PushdownState* sender)
		{
			std::cout << "State 1 sleeps" << std::endl;
		});
	PushdownState* state2 = new PushdownState([](PushdownState* sender, float dt)
		{
			std::cout << "State 2" << std::endl;
		},
		[](PushdownState* sender)
		{
			std::cout << "State 2 awakes" << std::endl;
		},
			[](PushdownState* sender)
		{
			std::cout << "State 2 sleeps" << std::endl;
		});
	PushdownState* states[3];

	states[0] = state0;
	states[1] = state1;
	states[2] = state2;
	for (int i = 0; i < 10; i++)
	{
		int num;
		std::cin >> num;
		char op;
		std::cin >> op;
		if (op == 'i')
		{
			machine->PushState(states[num]);
		}
		else if (op == 's')
		{
			
		}
		else
		{
			machine->PopState();
		}
		machine->Update(5);
	}

	delete machine;
	delete state0;
	delete state1;
	delete state2;
}

void TestNetworking() 
{
}

vector<Vector3> testNodes;

void TestPathfinding() 
{
	MazeData* maze = new MazeData();
	maze->Generate();
	for (const auto& group : maze->maze)
	{
		for (const auto& item : group)
		{
			std::cout << (item == WALL ? '*' : '-') << ' ';
		}
		std::cout << std::endl;
	}
	int from[2] = {1,1};
	int to[2] = {13,13};
	Node* path = maze->FindPath(from, to);
	auto* currentPath = path;
	while (currentPath != nullptr)
	{
		std::cout << currentPath->x << " " << currentPath->y << std::endl;
		auto* tempNode = currentPath;
		currentPath = currentPath->from;
		delete tempNode;
	}
}

void DisplayPathfinding()
{

}



/*

The main function should look pretty familar to you!
We make a window, and then go into a while loop that repeatedly
runs our 'game' until we press escape. Instead of making a 'renderer'
and updating it, we instead make a whole game, and repeatedly update that,
instead. 

This time, we've added some extra functionality to the window class - we can
hide or show the 

*/
int main() 
{
	// TestPushdown();
	TestStateMachine();
	TestPathfinding();
	Window*w = Window::CreateGameWindow("CSC8503 Game technology!", 1280, 720);

	if (!w->HasInitialised()) {
		return -1;
	}	

	//TestStateMachine();
	//TestNetworking();
	//TestPathfinding();
	
	w->ShowOSPointer(false);
	w->LockMouseToWindow(true);
	char c;
	std::cout << "Is this a server?";
	std::cin >> c;
	TutorialGame* g = new TutorialGame(c == 'y');

	while (w->UpdateWindow() && !Window::GetKeyboard()->KeyDown(KeyboardKeys::ESCAPE)) {
		float dt = w->GetTimer()->GetTimeDeltaSeconds();

		if (dt > 1.0f) {
			std::cout << "Skipping large time delta" << std::endl;
			continue; //must have hit a breakpoint or something to have a 1 second frame time!
		}
		if (Window::GetKeyboard()->KeyPressed(KeyboardKeys::PRIOR)) {
			w->ShowConsole(true);
		}
		if (Window::GetKeyboard()->KeyPressed(KeyboardKeys::NEXT)) {
			w->ShowConsole(false);
		}

		DisplayPathfinding();

		w->SetTitle("Gametech frame time:" + std::to_string(1000.0f * dt));

		g->UpdateGame(dt);
	}
	Window::DestroyGameWindow();
}