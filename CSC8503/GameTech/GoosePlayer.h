#pragma once
#include <iostream>
#include <vector>

#include "..\CSC8503Common\GameObject.h"
#include "..\CSC8503Common\StateMachine.h"
#include "..\CSC8503Common\StateTransition.h"
#include "..\CSC8503Common\State.h"
#include "AppleObject.h"
#include "DestinationObject.h"

namespace NCL 
{
	namespace CSC8503 
	{
		class GoosePlayer : public GameObject
		{
		public:
			GoosePlayer(Vector3 initPos, std::function<void(GoosePlayer*)> playerDelegate = [](GoosePlayer* sender) {});
			~GoosePlayer();

			virtual void OnCollisionBegin(GameObject* otherObject)
			{
				if (otherObject->GetPhysicsObject()->GetPhysicsLayer() == Layer::Apple)
				{

				}
				if (otherObject->GetPhysicsObject()->GetPhysicsLayer() == Layer::Water)
				{
					isInWater = true;
				}
				if (otherObject->GetPhysicsObject()->GetPhysicsLayer() == Layer::Enemy)
				{
					LoseAllApples();
				}
				if (otherObject->GetPhysicsObject()->GetPhysicsLayer() == Layer::SpawnPoint)
				{
					auto* dest = (DestinationObject*)otherObject;
					dest->AddApples(pickedApples);
					playerDelegate(this);
				}
			}

			virtual void OnCollisionEnd(GameObject* otherObject)
			{
				if (otherObject->GetPhysicsObject()->GetPhysicsLayer() == Layer::Water /*&&
					(transform.GetWorldPosition() - otherObject->GetTransform().GetWorldPosition()).Length() >= 2*/)
				{
					isInWater = false;
				}
			}

			void addApple(AppleObject* newApple)
			{
				pickedApples.push_back(newApple);
			}

			int GetNumberOfPickedApples()
			{
				return pickedApples.size();
			}

			void UpdateStateMachine();

			bool IsInWater()
			{
				return isInWater;
			}
			void ResetPlayer()
			{
				LoseAllApples();
				GetTransform().SetWorldPosition(initPos);
			}
			void LoseAllApples()
			{
				for (auto* item : pickedApples)
				{
					item->LoseApple();
				}
				pickedApples.clear();
			}
			const Vector3 initPos;
		private:
			vector<AppleObject*> pickedApples;

			StateMachine* gooseStateMachine;

			bool isInWater = false;

			const std::function<void(GoosePlayer*)> playerDelegate;
		};
	}
}

