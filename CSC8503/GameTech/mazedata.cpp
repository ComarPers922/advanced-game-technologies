#include "mazedata.h"

bool operator< (const Node& n1, const Node& n2)
{
	return (n1.gCost + n1.hCost) < (n2.gCost + n2.hCost);
}
bool operator> (const Node& n1, const Node& n2)
{
	return (n1.gCost + n1.hCost) > (n2.gCost + n2.hCost);
}
bool operator==(const Node& n1, const Node& n2)
{
	return n1.x == n2.x &&
		n1.y == n2.y;
}

MazeData::MazeData()
{
    srand(time(NULL));
    for(int i = 0 ; i < MazeData::height ; i ++)
    {
        for(int j = 0 ; j < MazeData::height ; j ++)
        {
            if(i%2 == 1 && j%2 == 1)
            {
                maze[i][j] = ROAD;
            }
            else
            {
                maze[i][j] = WALL;
            }
            visited[i][j] = false;
        }
    }
    //maze[entranceX][entranceY] = ROAD;
    //maze[exitX][exitY] = ROAD;
}

bool MazeData::inArea(int x, int y)
{
    return x >= 0 && x < height && y >= 0 && y < height;
}

void MazeData::reset()
{
    for(int i = 0 ; i < MazeData::height ; i ++)
    {
        for(int j = 0 ; j < MazeData::height ; j ++)
        {
            if(i%2 == 1 && j%2 == 1)
            {
                maze[i][j] = ROAD;
            }
            else
            {
                maze[i][j] = WALL;
            }
            visited[i][j] = false;
        }
    }
    //maze[entranceX][entranceY] = ROAD;
    //maze[exitX][exitY] = ROAD;
}

void MazeData::Generate()
{
	reset();
	RandomQueue<Position> queue;
	Position first(entranceX + 1, entranceY);
	queue.add(first);
	visited[first.x][first.y] = true;
	while (!queue.isEmpty())
	{
		auto currentPosition = queue.remove();
		for (int i = 0; i < 4; i++)
		{
			int newX = currentPosition.x + DIRECTIONS[i][0] * 2;
			int newY = currentPosition.y + DIRECTIONS[i][1] * 2;

			if (inArea(newX, newY)
				&& !visited[newX][newY]
				&& maze[newX][newY] == ROAD)
			{
				queue.add(Position(newX, newY));
				visited[newX][newY] = true;
				maze[currentPosition.x + DIRECTIONS[i][0]][currentPosition.y + DIRECTIONS[i][1]] = ROAD;
			}
		}
	}
	//int numberOfDestroyedWall = MazeData::height * std::max(height, 0);
	//for (int i = 0; i < numberOfDestroyedWall; i++)
	//{
	//	int x = rand() % MazeData::height - 2;
	//	int y = rand() % MazeData::height - 2;
	//	if (inArea(x, y) && x > 1 && y > 1)
	//	{
	//		maze[x][y] = ROAD;
	//	}
	//}
	for (int x = 1; x < height - 1; x++)
	{
		for (int y = 1; y < height - 1; y++)
		{
			if (maze[x][y] == WALL && rand() % 101 < 90)
			{
				maze[x][y] = ROAD;
			}
		}
	}
}

int* MazeData::GetValidNeighbor(const NCL::Maths::Vector2& pos)
{
	return GetValidNeighbor((int)pos.x, (int)pos.y);
}

int* MazeData::GetValidNeighbor(int x, int y)
{
	int* result = nullptr;
	if (!inArea(x, y))
	{
		return nullptr;
	}
	if (maze[x][y] == ROAD)
	{
		result = new int[2];
		result[0] = x;
		result[1] = y;
		return result;
	}
	for (const auto& group : DIRECTIONS)
	{
		if (maze[x + group[0]][y + group[1]] == ROAD)
		{
			result = new int[2];
			result[0] = x + group[0];
			result[1] = y + group[1];
		}
	}
	return result;
}

Node* MazeData::FindPath(int from[2], int to[2])
{
	if (!from || !to)
	{
		return nullptr;
	}
	if (!inArea(from[0], from[1]) || !inArea(to[0], to[1]))
	{
		return nullptr;
	}
	if (from[0] == to[0] && from[1] == to[1])
	{
		return new Node(from[0], to[0]);
	}
	if (from == nullptr || to == nullptr)
	{
		return nullptr;
	}
	if (maze[from[0]][from[1]] == WALL || maze[to[0]][to[1]] == WALL)
	{
		return nullptr;
	}
	unordered_set<Node, NodeHashFunction> closedSet;
	priority_queue<Node, std::vector<Node>, std::greater<Node>> openSet;
	openSet.push(Node(from[0], from[1]));
	while (!openSet.empty())
	{
		auto currentNode = openSet.top();
		openSet.pop();
		if (currentNode.x == to[0] && currentNode.y == to[1])
		{
			return &currentNode;
		}
		for (auto& item : DIRECTIONS)
		{
			Node* nextPoint = new Node(currentNode.x + item[0], currentNode.y + item[1]);
			if (!inArea(nextPoint->x, nextPoint->y))
			{
				continue;
			}
			if (closedSet.find(*nextPoint) != closedSet.end())
			{
				continue;
			}
			if (maze[nextPoint->x][nextPoint->y] == WALL)
			{
				closedSet.insert(*nextPoint);
				continue;
			}
			if (maze[nextPoint->x][nextPoint->y] == ROAD)
			{
				nextPoint->gCost = 1 + currentNode.gCost;
				nextPoint->hCost = abs(nextPoint->x - to[0]) + abs(nextPoint->y - to[1]);
				Node* fromNode = new Node(currentNode.x, currentNode.y);
				fromNode->from = currentNode.from;
				nextPoint->from = fromNode;
				if (nextPoint->x == to[0] &&
					nextPoint->y == to[1])
				{
					return nextPoint;
				}
				closedSet.insert(*nextPoint);
				openSet.push(*nextPoint);
			}
		}
	}
	return nullptr;
}
