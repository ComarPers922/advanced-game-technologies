#pragma once

class Position
{
public:
    Position(int X = 0, int Y = 0);
    int x, y;
    Position* from = nullptr;
    ~Position();
};
