#include "TutorialGame.h"
#include "../CSC8503Common/GameWorld.h"
#include "../../Plugins/OpenGLRendering/OGLMesh.h"
#include "../../Plugins/OpenGLRendering/OGLShader.h"
#include "../../Plugins/OpenGLRendering/OGLTexture.h"
#include "../../Common/TextureLoader.h"

#include "../CSC8503Common/PositionConstraint.h"

using namespace NCL;
using namespace CSC8503;

TutorialGame::TutorialGame(bool isServer):
	isServer(isServer)
{
	world		= new GameWorld();
	renderer	= new GameTechRenderer(*world);
	physics		= new PhysicsSystem(*world);

	forceMagnitude	= 10.0f;
	useGravity		= false;
	inSelectionMode = false;
	gameMap = new MazeData();
	gameMap->Generate();
	Debug::SetRenderer(renderer);
	for (int i = 2; i < gameMap->height; i++)
	{
		for (int j = 2; j < gameMap->height - 2; j++)
		{
			if (gameMap->maze[i][j] == ROAD)
			{
				vacantRoads.push_back(Node(i, j));
			}
		}
	}
	NetworkBase::Initialize();

	client = new GameClient();
	
	if (isServer)
	{
		server = new GameServer(NetworkBase::GetDefaultPort(), 4);
	}

	clientReceiver = new CustomReceiver([&](int type, GamePacket* packet, int source)
		{
			auto* data = (StringPacket*)packet;
			Debug::Print(data->GetStringFromData(), Vector2(0, 170));
		});

	client->RegisterPacketHandler(String_Message, (PacketReceiver*)clientReceiver);
	if (isServer)
	{
		serverReceiver = new CustomReceiver([&](int type, GamePacket* packet, int source)
			{
				static int highestScore = 0;
				auto* data = (StringPacket*)packet;
				int currentScore = atoi(data->GetStringFromData().c_str());
				highestScore = max(currentScore, highestScore);
				stringstream ss;
				ss << "Highest Score: " << highestScore;
				server->SendGlobalPacket(StringPacket(ss.str()));
			});
		serverHighscoreReceiver = new CustomReceiver([&](int type, GamePacket* packet, int source)
			{
				auto* data = (HighScorePacket*)packet;
				highScoreQueue.insert(data->GetScore());
			});
		server->RegisterPacketHandler(String_Message, (PacketReceiver*)serverReceiver);
		server->RegisterPacketHandler(High_Score, (PacketReceiver*) serverHighscoreReceiver);
	}
	client->Connect(127, 0, 0, 1, NetworkBase::GetDefaultPort());
	InitializeAssets();
}

/*

Each of the little demo scenarios used in the game uses the same 2 meshes, 
and the same texture and shader. There's no need to ever load in anything else
for this module, even in the coursework, but you can add it if you like!

*/
void TutorialGame::InitializeAssets() {
	auto loadFunc = [](const string& name, OGLMesh** into) {
		*into = new OGLMesh(name);
		(*into)->SetPrimitiveType(GeometryPrimitive::Triangles);
		(*into)->UploadToGPU();
	};

	loadFunc("cube.msh"		 , &cubeMesh);
	loadFunc("sphere.msh"	 , &sphereMesh);
	loadFunc("goose.msh"	 , &gooseMesh);
	loadFunc("CharacterA.msh", &keeperMesh);
	loadFunc("CharacterM.msh", &charA);
	loadFunc("CharacterF.msh", &charB);
	loadFunc("Apple.msh"	 , &appleMesh);

	basicTex	= (OGLTexture*)TextureLoader::LoadAPITexture("checkerboard.png");
	basicShader = new OGLShader("GameTechVert.glsl", "GameTechFrag.glsl");



	InitCamera();
	InitWorld();
}

TutorialGame::~TutorialGame()	
{
	delete cubeMesh;
	delete sphereMesh;
	delete gooseMesh;
	delete basicTex;
	delete basicShader;

	delete physics;
	delete renderer;
	delete world;

	delete playerObject;
	delete destObject;
	delete gameMap;

	delete gameMap;

	delete client;
	delete server;
}

void TutorialGame::UpdateGame(float dt)
{
#pragma region Original Update
	//if (Window::GetKeyboard()->KeyPressed(KeyboardKeys::P))
	//{
	//	playerObject->GetTransform().SetWorldPosition(playerObject->initPos);
	//}


	//for (auto* item : apples)
	//{
	//	item->UpdateStateMachine();
	//}
	//if (!destObject->GetGameOver())
	//{
	//	for (auto* item : enemies)
	//	{
	//		item->UpdateStateMachine();
	//		if (item->GetChasingPath().size() < 2)
	//		{
	//			continue;
	//		}
	//		for (int i = 0; i < item->GetChasingPath().size() - 1; i++)
	//		{
	//			Debug::DrawLine(item->GetChasingPath()[i], item->GetChasingPath()[i + 1], Vector4(1, 0, 0, 1));
	//		}
	//		Vector3 from = item->GetTransform().GetWorldPosition();
	//		Vector3 to = item->GetChasingPath()[item->GetChasingPath().size() - 2];
	//		float speed = 5 - (item->IsInWater() ? 2 : 0);
	//		Vector3 delta = (to - from).Normalized();
	//		item->GetTransform().SetWorldPosition(from + delta * speed * dt);
	//		
	//		Vector3 forward = item->GetTransform().GetLocalOrientation() * Vector3(0, 0, 1);
	//		forward.Normalize();

	//		Vector2 forward2 = Vector2(forward.x, forward.z);
	//		Vector2 delta2 = Vector2(delta.x, delta.z);

	//		float angle = acos(Vector2::Dot(delta2, forward2)) + 90;

	//		Quaternion currentDir = item->GetTransform().GetLocalOrientation();
	//		Quaternion dirShouldBe = Quaternion::AxisAngleToQuaterion(Vector3(0,1,0), currentDir.y + RadiansToDegrees(angle));
	//		Quaternion finalDir = Quaternion::Slerp(currentDir, dirShouldBe, 0.70f);
	//		item->GetTransform().SetLocalOrientation(finalDir);
	//	}
	//}
	//if (!inSelectionMode)
	//{
	//	world->GetMainCamera()->CameraFollow(playerObject->GetTransform().GetWorldPosition(), 7, 12, playerObject->GetTransform().GetWorldOrientation());

	//	Vector3 playerPosition = playerObject->GetTransform().GetWorldPosition();
	//	float force = 25;
	//	force -= playerObject->GetNumberOfPickedApples() * 3;
	//	if (playerObject->IsInWater())
	//	{
	//		force = 5;
	//	}
	//	force = max(force, 5);
	//	if (Window::GetKeyboard()->KeyDown(KeyboardKeys::SHIFT) && !playerObject->IsInWater())
	//	{
	//		force *= 2;
	//	}
	//	if (Window::GetKeyboard()->KeyDown(KeyboardKeys::W))
	//	{
	//		Vector3 forward = playerObject->GetTransform().GetWorldOrientation() * Vector3(0,0,1);
	//		forward.Normalize();
	//		playerObject->GetPhysicsObject()->AddForce(forward * force);
	//	}
	//	if (Window::GetKeyboard()->KeyDown(KeyboardKeys::S)) 
	//	{
	//		Vector3 forward = playerObject->GetTransform().GetWorldOrientation() * Vector3(0, 0, 1);
	//		forward.Normalize();
	//		playerObject->GetPhysicsObject()->AddForce(-forward * force);
	//	}
	//	Quaternion playerOrientation = playerObject->GetTransform().GetLocalOrientation();
	//	if (Window::GetKeyboard()->KeyDown(KeyboardKeys::A))
	//	{
	//		playerObject->GetPhysicsObject()->AddTorque(Vector3(0,1,0));
	//	}
	//	if (Window::GetKeyboard()->KeyDown(KeyboardKeys::D)) 
	//	{
	//		playerObject->GetPhysicsObject()->AddTorque(Vector3(0, -1, 0));
	//	}
	//	playerObject->GetTransform().SetLocalOrientation(Quaternion::EulerAnglesToQuaternion(0, playerOrientation.ToEuler().y,0));
	//}
	//else
	//{
	//	world->GetMainCamera()->UpdateCamera(dt);
	//}
		//if (lockedObject != nullptr)
	//{
	//	LockedCameraMovement();
	//}

	// UpdateKeys();



	//SelectObject();
	//MoveSelectedObject();

	//UpdateConstraint(dt);
	//world->UpdateWorld(dt);
	//renderer->Update(dt);
	//physics->Update(dt);

	//Debug::FlushRenderables();
	//renderer->Render();
#pragma endregion
	if (isServer)
	{
		server->UpdateServer();
		Debug::Print("Press B to save high score table!", Vector2(0, 200));
		if (Window::GetKeyboard()->KeyPressed(KeyboardKeys::B))
		{
			std::ofstream output_stream;
			stringstream ss;
			ss << "High scores: ";
			bool ok = false;
			for (const auto& item : highScoreQueue)
			{
				ss << (ok ? "" : " ")<< item;
				ok = true;
			}
			output_stream.open(".\score.txt");
			output_stream << ss.str();
			output_stream.close();
		}
	}
	stringstream ss;
	ss << destObject->GetTotalPoints();
	client->SendPacket(StringPacket(ss.str()));
	client->UpdateClient();
	Debug::Print("Press ESC to exit the game", Vector2(0, 150), Vector4(1, 1, 1, 1));
	pushdownStateMachine->Update(dt);


	Debug::FlushRenderables();
	renderer->Render();
}

void TutorialGame::UpdateKeys() {
	if (Window::GetKeyboard()->KeyPressed(KeyboardKeys::F2)) {
		InitCamera(); //F2 will reset the camera to a specific default place
	}

	//Running certain physics updates in a consistent order might cause some
	//bias in the calculations - the same objects might keep 'winning' the constraint
	//allowing the other one to stretch too much etc. Shuffling the order so that it
	//is random every frame can help reduce such bias.
	if (Window::GetKeyboard()->KeyPressed(KeyboardKeys::F9)) {
		world->ShuffleConstraints(true);
	}
	if (Window::GetKeyboard()->KeyPressed(KeyboardKeys::F10)) {
		world->ShuffleConstraints(false);
	}

	if (Window::GetKeyboard()->KeyPressed(KeyboardKeys::F7)) {
		world->ShuffleObjects(true);
	}
	if (Window::GetKeyboard()->KeyPressed(KeyboardKeys::F8)) {
		world->ShuffleObjects(false);
	}

	if (lockedObject) {
		LockedObjectMovement();
	}
	else {
		DebugObjectMovement();
	}
}

void TutorialGame::LockedObjectMovement() {
	Matrix4 view		= world->GetMainCamera()->BuildViewMatrix();
	Matrix4 camWorld	= view.Inverse();

	Vector3 rightAxis = Vector3(camWorld.GetColumn(0)); //view is inverse of model!

	//forward is more tricky -  camera forward is 'into' the screen...
	//so we can take a guess, and use the cross of straight up, and
	//the right axis, to hopefully get a vector that's good enough!

	Vector3 fwdAxis = Vector3::Cross(Vector3(0, 1, 0), rightAxis);

	if (Window::GetKeyboard()->KeyDown(KeyboardKeys::LEFT)) {
		selectionObject->GetPhysicsObject()->AddForce(-rightAxis);
	}

	if (Window::GetKeyboard()->KeyDown(KeyboardKeys::RIGHT)) {
		selectionObject->GetPhysicsObject()->AddForce(rightAxis);
	}

	if (Window::GetKeyboard()->KeyDown(KeyboardKeys::UP)) {
		selectionObject->GetPhysicsObject()->AddForce(fwdAxis);
	}

	if (Window::GetKeyboard()->KeyDown(KeyboardKeys::DOWN)) {
		selectionObject->GetPhysicsObject()->AddForce(-fwdAxis);
	}
}

void  TutorialGame::LockedCameraMovement() {
	if (lockedObject != nullptr) {
		Vector3 objPos = lockedObject->GetTransform().GetWorldPosition();
		Vector3 camPos = objPos + lockedOffset;

		Matrix4 temp = Matrix4::BuildViewMatrix(camPos, objPos, Vector3(0, 1, 0));

		Matrix4 modelMat = temp.Inverse();

		Quaternion q(modelMat);
		Vector3 angles = q.ToEuler(); //nearly there now!

		world->GetMainCamera()->SetPosition(camPos);
		world->GetMainCamera()->SetPitch(angles.x);
		world->GetMainCamera()->SetYaw(angles.y);
	}
}


void TutorialGame::DebugObjectMovement() {
//If we've selected an object, we can manipulate it with some key presses
	if (inSelectionMode && selectionObject) {
		//Twist the selected object!
		if (Window::GetKeyboard()->KeyDown(KeyboardKeys::LEFT)) {
			selectionObject->GetPhysicsObject()->AddTorque(Vector3(-10, 0, 0));
		}

		if (Window::GetKeyboard()->KeyDown(KeyboardKeys::RIGHT)) {
			selectionObject->GetPhysicsObject()->AddTorque(Vector3(10, 0, 0));
		}

		if (Window::GetKeyboard()->KeyDown(KeyboardKeys::NUM7)) {
			selectionObject->GetPhysicsObject()->AddTorque(Vector3(0, 10, 0));
		}

		if (Window::GetKeyboard()->KeyDown(KeyboardKeys::NUM8)) {
			selectionObject->GetPhysicsObject()->AddTorque(Vector3(0, -10, 0));
		}

		if (Window::GetKeyboard()->KeyDown(KeyboardKeys::RIGHT)) {
			selectionObject->GetPhysicsObject()->AddTorque(Vector3(10, 0, 0));
		}

		if (Window::GetKeyboard()->KeyDown(KeyboardKeys::UP)) {
			selectionObject->GetPhysicsObject()->AddForce(Vector3(0, 0, -10));
		}

		if (Window::GetKeyboard()->KeyDown(KeyboardKeys::DOWN)) {
			selectionObject->GetPhysicsObject()->AddForce(Vector3(0, 0, 10));
		}

		if (Window::GetKeyboard()->KeyDown(KeyboardKeys::NUM5)) {
			selectionObject->GetPhysicsObject()->AddForce(Vector3(0, -10, 0));
		}
	}
}

/*

Every frame, this code will let you perform a raycast, to see if there's an object
underneath the cursor, and if so 'select it' into a pointer, so that it can be
manipulated later. Pressing Q will let you toggle between this behaviour and instead
letting you move the camera around.

*/
bool TutorialGame::SelectObject() 
{
	static Vector4 originalColor = Vector4(0, 0, 0, 1);
	if (Window::GetKeyboard()->KeyPressed(KeyboardKeys::Q))
	{
		if (selectionObject)
		{	//set color to deselected;
			selectionObject->GetRenderObject()->SetColor(originalColor);
			selectionObject = nullptr;
		}
		inSelectionMode = !inSelectionMode;
		if (inSelectionMode) 
		{
			Window::GetWindow()->ShowOSPointer(true);
			Window::GetWindow()->LockMouseToWindow(false);
		}
		else
		{
			Window::GetWindow()->ShowOSPointer(false);
			Window::GetWindow()->LockMouseToWindow(true);
		}
	}
	if (inSelectionMode) {
		renderer->DrawString("Press Q to change to camera mode!", Vector2(10, 0));

		if (Window::GetMouse()->ButtonDown(NCL::MouseButtons::LEFT)) {
			if (selectionObject) {	//set color to deselected;
				selectionObject->GetRenderObject()->SetColor(originalColor);
				selectionObject = nullptr;
			}

			Ray ray = CollisionDetection::BuildRayFromMouse(*world->GetMainCamera());

			RayCollision closestCollision;
			if (world->Raycast(ray, closestCollision, true)) {
				selectionObject = (GameObject*)closestCollision.node;
				originalColor = selectionObject->GetRenderObject()->GetColor();
				selectionObject->GetRenderObject()->SetColor(Vector4(0.5, 1, 0.5, 1));
				return true;
			}
			else {
				return false;
			}
		}
		if (Window::GetKeyboard()->KeyPressed(NCL::KeyboardKeys::L)) {
			if (selectionObject) {
				if (lockedObject == selectionObject) {
					lockedObject = nullptr;
				}
				else {
					lockedObject = selectionObject;
				}
			}
		}
	}
	else {
		renderer->DrawString("Press Q to change to select mode!", Vector2(10, 0));
	}
	return false;
}

/*
If an object has been clicked, it can be pushed with the right mouse button, by an amount
determined by the scroll wheel. In the first tutorial this won't do anything, as we haven't
added linear motion into our physics system. After the second tutorial, objects will move in a straight
line - after the third, they'll be able to twist under torque aswell.
*/

void TutorialGame::MoveSelectedObject() 
{
	// renderer->DrawString("Click Force: " + std::to_string(forceMagnitude), Vector2(10, 20));
	// forceMagnitude += Window::GetMouse()->GetWheelMovement() * 100.0f;

	if (!selectionObject)
	{
		return;
	}
	renderer->DrawString(selectionObject->printedInformation(), Vector2(0, 250), Vector4(1, 1, 1, 1));
	//if (Window::GetMouse()->ButtonPressed(NCL::MouseButtons::RIGHT))
	//{
	//	Ray ray = CollisionDetection::BuildRayFromMouse(*world->GetMainCamera());

	//	RayCollision closestCollision;
	//	if (world->Raycast(ray, closestCollision, true))
	//	{
	//		if (closestCollision.node == selectionObject)
	//		{
	//			// selectionObject->GetPhysicsObject()->AddForce(ray.GetDirection() * forceMagnitude);
	//			selectionObject->GetPhysicsObject()->AddForceAtPosition(ray.GetDirection() * forceMagnitude, closestCollision.collidedAt);
	//		}
	//	}
	//}
}

void TutorialGame::InitCamera() {
	world->GetMainCamera()->SetNearPlane(0.5f);
	world->GetMainCamera()->SetFarPlane(500.0f);
	world->GetMainCamera()->SetPitch(-15.0f);
	world->GetMainCamera()->SetYaw(315.0f);
	world->GetMainCamera()->SetPosition(Vector3(-60, 40, 60));
	lockedObject = nullptr;
}

void TutorialGame::InitWorld() 
{
	useGravity = true;
	pushdownStateMachine->PushState(gameState);
	world->ClearAndErase();
	physics->Clear();

	//InitMixedGridWorld(10, 10, 3.5f, 3.5f);
	InitMixedGridWorld(10, 10, 2, 2);
	// AddGooseToWorld(Vector3(30, 0.2, 0));

	{
		int randIndex = rand() % vacantRoads.size();
		auto pos = vacantRoads[randIndex];
		vacantRoads.erase(vacantRoads.begin() + randIndex);
		AddGooseToWorld(Vector3(pos.x, 0, pos.y) - WORLD_ORIGIN_POINT);
		AddSpawnPoint(Vector3(pos.x, -0.7, pos.y) - WORLD_ORIGIN_POINT, Vector3(3, 1, 3), 0);
	}
	for (int i = 0; i < NUMBER_OF_ENEMIES; i++)
	{
		int randIndex = rand() % vacantRoads.size();
		auto pos = vacantRoads[randIndex];
		vacantRoads.erase(vacantRoads.begin() + randIndex);
		Vector3 initPos = Vector3(pos.x, 1.8, pos.y) * 2 - WORLD_ORIGIN_POINT;
		auto* newEnemy = new EnemyObject(playerObject, gameMap, initPos);
		enemies.push_back(newEnemy);
		newEnemy->GetTransform().SetWorldPosition(initPos);
		AddParkKeeperToWorld(newEnemy);
	}
	for (int i = 0; i < NUMBER_OF_APPLES; i++)
	{
		int randIndex = rand() % vacantRoads.size();
		auto pos = vacantRoads[randIndex];
		vacantRoads.erase(vacantRoads.begin() + randIndex);
		AddAppleToWorld(Vector3(pos.x, 0.5, pos.y) * 2 - WORLD_ORIGIN_POINT, i);
	}
	for (int i = 0; i < NUMBER_OF_WATERS; i++)
	{
		int randIndex = rand() % vacantRoads.size();
		auto pos = vacantRoads[randIndex];
		vacantRoads.erase(vacantRoads.begin() + randIndex);
		AddSoilToWorld(Vector3(pos.x, 0, pos.y) * 2 - Vector3(0, 0.5, 0) - WORLD_ORIGIN_POINT, Vector3(2, 1, 2), 0);
	}
	//AddParkKeeperToWorld(Vector3(40, 2, 0));
	// AddCharacterToWorld(Vector3(45, 2, 0));

	// AddFloorToWorld(Vector3(0, -2, 0));
}

//From here on it's functions to add in objects to the world!

/*

A single function to add a large immoveable cube to the bottom of our world

*/
GameObject* TutorialGame::AddFloorToWorld(const Vector3& position) {
	GameObject* floor = new GameObject();

	Vector3 floorSize = Vector3(100, 2, 100);
	AABBVolume* volume = new AABBVolume(floorSize);
	floor->SetBoundingVolume((CollisionVolume*)volume);
	floor->GetTransform().SetWorldScale(floorSize);
	floor->GetTransform().SetWorldPosition(position);

	floor->SetRenderObject(new RenderObject(&floor->GetTransform(), cubeMesh, nullptr, basicShader));
	floor->SetPhysicsObject(new PhysicsObject(&floor->GetTransform(), floor->GetBoundingVolume(), PhysicsType::Static, Layer::Floor));
	floor->GetRenderObject()->SetColor(Vector4(0.47, 0.48, 0.49, 1));

	floor->GetPhysicsObject()->SetInverseMass(0);
	floor->GetPhysicsObject()->InitCubeInertia();
	floor->GetPhysicsObject()->SetElasticity(0);

	world->AddGameObject(floor);

	return floor;
}

/*

Builds a game object that uses a sphere mesh for its graphics, and a bounding sphere for its
rigid body representation. This and the cube function will let you build a lot of 'simple' 
physics worlds. You'll probably need another function for the creation of OBB cubes too.

*/
GameObject* TutorialGame::AddSphereToWorld(const Vector3& position, float radius, float inverseMass) {
	GameObject* sphere = new GameObject();

	Vector3 sphereSize = Vector3(radius, radius, radius);
	SphereVolume* volume = new SphereVolume(radius);
	sphere->SetBoundingVolume((CollisionVolume*)volume);
	sphere->GetTransform().SetWorldScale(sphereSize);
	sphere->GetTransform().SetWorldPosition(position);

	sphere->SetRenderObject(new RenderObject(&sphere->GetTransform(), sphereMesh, basicTex, basicShader));
	sphere->SetPhysicsObject(new PhysicsObject(&sphere->GetTransform(), sphere->GetBoundingVolume(), PhysicsType::Dynamic));

	sphere->GetPhysicsObject()->SetInverseMass(inverseMass);
	sphere->GetPhysicsObject()->InitSphereInertia();

	world->AddGameObject(sphere);

	return sphere;
}

GameObject* TutorialGame::AddWallToWorld(const Vector3& position, Vector3 dimensions, float inverseMass)
{
	GameObject* wall = new GameObject();

	AABBVolume* volume = new AABBVolume(dimensions);

	wall->SetBoundingVolume((CollisionVolume*)volume);

	wall->GetTransform().SetWorldPosition(position - WORLD_ORIGIN_POINT);
	wall->GetTransform().SetWorldScale(dimensions);

	wall->SetRenderObject(new RenderObject(&wall->GetTransform(), cubeMesh, nullptr, basicShader));
	wall->SetPhysicsObject(new PhysicsObject(&wall->GetTransform(), wall->GetBoundingVolume(), PhysicsType::Static, Layer::Wall));
	wall->GetRenderObject()->SetColor(Vector4(0.31,0.86,0.39,1));

	wall->GetPhysicsObject()->SetInverseMass(inverseMass);
	wall->GetPhysicsObject()->InitCubeInertia();
	wall->GetPhysicsObject()->SetElasticity(0);

	world->AddGameObject(wall);

	return wall;
}

GameObject* TutorialGame::AddGooseToWorld(const Vector3& position)
{
	float size			= 0.5f;
	float inverseMass	= 1.0f;

	NCL::CSC8503::GoosePlayer* goose = new NCL::CSC8503::GoosePlayer(position, [&](GoosePlayer* sender)
		{
			for (auto* item : constraintList)
			{
				delete item;
			}
			constraintList.clear();
		});

	playerObject = goose;
	SphereVolume* volume = new SphereVolume(size);
	goose->SetBoundingVolume((CollisionVolume*)volume);

	goose->GetTransform().SetWorldScale(Vector3(size,size,size) );
	goose->GetTransform().SetWorldPosition(position);

	goose->SetRenderObject(new RenderObject(&goose->GetTransform(), gooseMesh, nullptr, basicShader));
	goose->SetPhysicsObject(new PhysicsObject(&goose->GetTransform(), goose->GetBoundingVolume(), PhysicsType::Dynamic));

	goose->GetPhysicsObject()->SetInverseMass(inverseMass);
	goose->GetPhysicsObject()->InitSphereInertia();
	goose->GetPhysicsObject()->SetPhysicsLayer(Layer::Player);

	world->AddGameObject(goose);

	return goose;
}

GameObject* TutorialGame::AddParkKeeperToWorld(EnemyObject* keeper)
{
	float meshSize = 4.0f;
	float inverseMass = 0.01f;

	AABBVolume* volume = new AABBVolume(Vector3(0.3, 0.9, 0.3) * meshSize);
	keeper->SetBoundingVolume((CollisionVolume*)volume);

	keeper->GetTransform().SetWorldScale(Vector3(meshSize, meshSize, meshSize));
	keeper->GetTransform().SetWorldPosition(keeper->GetTransform().GetWorldPosition());

	keeper->SetRenderObject(new RenderObject(&keeper->GetTransform(), keeperMesh, nullptr, basicShader));
	keeper->SetPhysicsObject(new PhysicsObject(&keeper->GetTransform(), keeper->GetBoundingVolume(), PhysicsType::Dynamic,Layer::Enemy, false));

	keeper->GetPhysicsObject()->SetInverseMass(inverseMass);
	keeper->GetPhysicsObject()->InitCubeInertia();

	world->AddGameObject(keeper);

	return keeper;
}

GameObject* NCL::CSC8503::TutorialGame::AddSpawnPoint(const Vector3& position, Vector3 dimensions, float inverseMass)
{
	DestinationObject* spawnPoint = new DestinationObject();
	destObject = spawnPoint;

	AABBVolume* volume = new AABBVolume(dimensions);

	spawnPoint->SetBoundingVolume((CollisionVolume*)volume);

	spawnPoint->GetTransform().SetWorldPosition(position);
	spawnPoint->GetTransform().SetWorldScale(dimensions);

	spawnPoint->SetRenderObject(new RenderObject(&spawnPoint->GetTransform(), cubeMesh, nullptr, basicShader));
	spawnPoint->SetPhysicsObject(new PhysicsObject(&spawnPoint->GetTransform(), spawnPoint->GetBoundingVolume(), PhysicsType::Static, Layer::SpawnPoint, true));
	spawnPoint->GetRenderObject()->SetColor(Vector4(0.9, 0.7, 0.2, 1));

	spawnPoint->GetPhysicsObject()->SetInverseMass(inverseMass);
	spawnPoint->GetPhysicsObject()->InitCubeInertia();
	spawnPoint->GetPhysicsObject()->SetElasticity(0);

	world->AddGameObject(spawnPoint);

	return spawnPoint;
}

GameObject* TutorialGame::AddParkKeeperToWorld(const Vector3& position)
{
	float meshSize = 4.0f;
	float inverseMass = 0.5f;

	GameObject* keeper = new GameObject();

	AABBVolume* volume = new AABBVolume(Vector3(0.3, 0.9f, 0.3) * meshSize);
	keeper->SetBoundingVolume((CollisionVolume*)volume);

	keeper->GetTransform().SetWorldScale(Vector3(meshSize, meshSize, meshSize));
	keeper->GetTransform().SetWorldPosition(position);

	keeper->SetRenderObject(new RenderObject(&keeper->GetTransform(), keeperMesh, nullptr, basicShader));
	keeper->SetPhysicsObject(new PhysicsObject(&keeper->GetTransform(), keeper->GetBoundingVolume(), PhysicsType::Dynamic));

	keeper->GetPhysicsObject()->SetInverseMass(inverseMass);
	keeper->GetPhysicsObject()->InitCubeInertia();

	world->AddGameObject(keeper);

	return keeper;
}

GameObject* TutorialGame::AddCharacterToWorld(const Vector3& position) {
	float meshSize = 4.0f;
	float inverseMass = 0.5f;

	auto pos = keeperMesh->GetPositionData();

	Vector3 minVal = pos[0];
	Vector3 maxVal = pos[0];

	for (auto& i : pos) {
		maxVal.y = max(maxVal.y, i.y);
		minVal.y = min(minVal.y, i.y);
	}

	GameObject* character = new GameObject();

	float r = rand() / (float)RAND_MAX;


	AABBVolume* volume = new AABBVolume(Vector3(0.3, 0.9f, 0.3) * meshSize);
	character->SetBoundingVolume((CollisionVolume*)volume);

	character->GetTransform().SetWorldScale(Vector3(meshSize, meshSize, meshSize));
	character->GetTransform().SetWorldPosition(position);

	character->SetRenderObject(new RenderObject(&character->GetTransform(), r > 0.5f ? charA : charB, nullptr, basicShader));
	character->SetPhysicsObject(new PhysicsObject(&character->GetTransform(), character->GetBoundingVolume(), PhysicsType::Dynamic));

	character->GetPhysicsObject()->SetInverseMass(inverseMass);
	character->GetPhysicsObject()->InitCubeInertia();

	world->AddGameObject(character);

	return character;
}

GameObject* TutorialGame::AddAppleToWorld(const Vector3& position, int appleIndex)
{
	int point = Clamp(rand() % 20, 5, 15);
	AppleObject* apple = new AppleObject(playerObject, position, destObject, point,
		[&](AppleObject* sender)
		{
			PositionConstraint* constraint = new PositionConstraint(playerObject, sender, 1.0f);
			constraintList.emplace_back(constraint);
		});
	apples[appleIndex] = apple;

	SphereVolume* volume = new SphereVolume(0.7f);
	apple->SetBoundingVolume((CollisionVolume*)volume);
	apple->GetTransform().SetWorldScale(Vector3(2, 2, 2));
	apple->GetTransform().SetWorldPosition(position);

	apple->SetRenderObject(new RenderObject(&apple->GetTransform(), appleMesh, nullptr, basicShader));
	apple->SetPhysicsObject(new PhysicsObject(&apple->GetTransform(), apple->GetBoundingVolume(), PhysicsType::Static, Layer::Apple, false));
	apple->GetRenderObject()->SetColor(Vector4(1,Clamp((apple->Points - 5) / 10.0f, 0.0f, 1.0f),0.2,1));

	apple->GetPhysicsObject()->SetInverseMass(10000000);
	apple->GetPhysicsObject()->InitSphereInertia();

	world->AddGameObject(apple);

	return apple;
}

GameObject* NCL::CSC8503::TutorialGame::AddSoilToWorld(const Vector3& position, Vector3 dimensions, float inverseMass)
{
	GameObject* soil = new GameObject();

	AABBVolume* volume = new AABBVolume(dimensions);

	soil->SetBoundingVolume((CollisionVolume*)volume);

	soil->GetTransform().SetWorldPosition(position);
	soil->GetTransform().SetWorldScale(dimensions);

	soil->SetRenderObject(new RenderObject(&soil->GetTransform(), cubeMesh, nullptr, basicShader));
	soil->SetPhysicsObject(new PhysicsObject(&soil->GetTransform(), soil->GetBoundingVolume(), PhysicsType::Static, Layer::Water, true));
	soil->GetRenderObject()->SetColor(Vector4(.75, .25, 0, 1));

	soil->GetPhysicsObject()->SetInverseMass(inverseMass);
	soil->GetPhysicsObject()->InitCubeInertia();
	soil->GetPhysicsObject()->SetElasticity(0);

	world->AddGameObject(soil);

	return soil;
}

Vector2 NCL::CSC8503::TutorialGame::WorldPosToNodePos(float x, float y)
{
	return Vector2((int)(x / 2 + 0.5), (int)(y / 2 + 0.5));
}

void NCL::CSC8503::TutorialGame::UpdateConstraint(float dt)
{
	if (constraintList.size())
	{
		//std::vector<PositionConstraint*>::const_iterator first = constraintList.begin();
		//for (; first < constraintList.end(); first++)
		//{
		//	(*first)->UpdateConstraint(dt);
		//}
		for (auto* item : constraintList)
		{
			item->UpdateConstraint(dt);
		}
	}
}

void TutorialGame::InitSphereGridWorld(int numRows, int numCols, float rowSpacing, float colSpacing, float radius) {
	for (int x = 0; x < numCols; ++x) {
		for (int z = 0; z < numRows; ++z) {
			Vector3 position = Vector3(x * colSpacing, 10.0f, z * rowSpacing);
			AddSphereToWorld(position, radius, 1.0f);
		}
	}
	AddFloorToWorld(Vector3(0, -2, 0));
}

void TutorialGame::InitMixedGridWorld(int numRows, int numCols, float rowSpacing, float colSpacing) {
	float sphereRadius = 1.0f;
	Vector3 cubeDims = Vector3(1, 1, 1);

	//for (int x = 0; x < numCols; ++x) {
	//	for (int z = 0; z < numRows; ++z) {
	//		Vector3 position = Vector3(x * colSpacing, 10.0f, z * rowSpacing);

	//		if (rand() % 2) {
	//			AddCubeToWorld(position, cubeDims);
	//		}
	//		else {
	//			AddSphereToWorld(position, sphereRadius);
	//		}
	//	}
	//}
	for (int x = 0; x < gameMap->height; x++)
	{
		for (int z = 0; z < gameMap->height; z++)
		{
			if (gameMap->maze[x][z] != WALL)
			{
				continue;
			}
			Vector3 position = Vector3(x * colSpacing, 1, z * rowSpacing);
			AddWallToWorld(position, cubeDims, 0);
		}
	}
	AddFloorToWorld(Vector3(0, -2, 0));
}

void TutorialGame::InitCubeGridWorld(int numRows, int numCols, float rowSpacing, float colSpacing, const Vector3& cubeDims) {
	for (int x = 1; x < numCols+1; ++x) {
		for (int z = 1; z < numRows+1; ++z) {
			Vector3 position = Vector3(x * colSpacing, 10.0f, z * rowSpacing);
			AddWallToWorld(position, cubeDims, 1.0f);
		}
	}
	AddFloorToWorld(Vector3(0, -2, 0));
}

void TutorialGame::BridgeConstraintTest() {
	Vector3 cubeSize = Vector3(8, 8, 8);

	float	invCubeMass = 5;
	int		numLinks	= 25;
	float	maxDistance	= 30;
	float	cubeDistance = 20;

	Vector3 startPos = Vector3(500, 1000, 500);

	GameObject* start = AddWallToWorld(startPos + Vector3(0, 0, 0), cubeSize, 0);

	GameObject* end = AddWallToWorld(startPos + Vector3((numLinks + 2) * cubeDistance, 0, 0), cubeSize, 0);

	GameObject* previous = start;

	for (int i = 0; i < numLinks; ++i) {
		GameObject* block = AddWallToWorld(startPos + Vector3((i + 1) * cubeDistance, 0, 0), cubeSize, invCubeMass);
		PositionConstraint* constraint = new PositionConstraint(previous, block, maxDistance);
		world->AddConstraint(constraint);
		previous = block;
	}

	PositionConstraint* constraint = new PositionConstraint(previous, end, maxDistance);
	world->AddConstraint(constraint);
}

void TutorialGame::SimpleGJKTest() {
	Vector3 dimensions		= Vector3(5, 5, 5);
	Vector3 floorDimensions = Vector3(100, 2, 100);

	GameObject* fallingCube = AddWallToWorld(Vector3(0, 20, 0), dimensions, 10.0f);
	GameObject* newFloor	= AddWallToWorld(Vector3(0, 0, 0), floorDimensions, 0.0f);

	delete fallingCube->GetBoundingVolume();
	delete newFloor->GetBoundingVolume();

	fallingCube->SetBoundingVolume((CollisionVolume*)new OBBVolume(dimensions));
	newFloor->SetBoundingVolume((CollisionVolume*)new OBBVolume(floorDimensions));

}

