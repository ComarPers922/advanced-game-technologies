#pragma once
#include <sstream>
#include <set>
#include <fstream>
#include "GameTechRenderer.h"
#include "../CSC8503Common/PhysicsSystem.h"
#include "mazedata.h"

#include "GoosePlayer.h"
#include "AppleObject.h"
#include "EnemyObject.h"
#include "DestinationObject.h"

#include "../CSC8503Common/Constraint.h"
#include "../CSC8503Common/PositionConstraint.h"
#include "../../Common/Maths.h"
#include "../CSC8503Common/PushdownMachine.h"
#include "../CSC8503Common/PushdownState.h"

#include "../CSC8503Common/NetworkBase.h"
#include "../CSC8503Common/NetworkObject.h"
#include "../CSC8503Common/NetworkState.h"
#include "../CSC8503Common/GameClient.h"
#include "../CSC8503Common/GameServer.h"

#define WORLD_ORIGIN_POINT Vector3(WORLD_ORIGIN_OFFSET_X, 0, WORLD_ORIGIN_OFFSET_Z)

constexpr auto WORLD_ORIGIN_OFFSET_X = 50;
constexpr auto WORLD_ORIGIN_OFFSET_Z = 50;

constexpr auto NUMBER_OF_APPLES = 20;
constexpr auto NUMBER_OF_WATERS = 50;
constexpr auto NUMBER_OF_ENEMIES = 5;

constexpr auto GAME_TIME = 300;

using namespace NCL::Maths;

namespace NCL {
	namespace CSC8503 {
		class TutorialGame		{
		public:
			TutorialGame(bool isServer = false);
			~TutorialGame();

			virtual void UpdateGame(float dt);

		protected:
			GameClient* client ;
			GameServer* server = nullptr;
			CustomReceiver* clientReceiver;
			CustomReceiver* serverReceiver;
			CustomReceiver* serverHighscoreReceiver;

			void InitializeAssets();

			void InitCamera();
			void UpdateKeys();

			void InitWorld();

			/*
			These are some of the world/object creation functions I created when testing the functionality
			in the module. Feel free to mess around with them to see different objects being created in different
			test scenarios (constraints, collision types, and so on). 
			*/
			void InitSphereGridWorld(int numRows, int numCols, float rowSpacing, float colSpacing, float radius);
			void InitMixedGridWorld(int numRows, int numCols, float rowSpacing, float colSpacing);
			void InitCubeGridWorld(int numRows, int numCols, float rowSpacing, float colSpacing, const Vector3& cubeDims);
			void BridgeConstraintTest();
			void SimpleGJKTest();

			bool SelectObject();
			void MoveSelectedObject();
			void DebugObjectMovement();
			void LockedObjectMovement();
			void LockedCameraMovement();

			GameObject* AddFloorToWorld(const Vector3& position);
			GameObject* AddSphereToWorld(const Vector3& position, float radius, float inverseMass = 10.0f);
			GameObject* AddWallToWorld(const Vector3& position, Vector3 dimensions, float inverseMass = 10.0f);
			//IT'S HAPPENING
			GameObject* AddGooseToWorld(const Vector3& position);
			GameObject* AddParkKeeperToWorld(const Vector3& position);
			GameObject* AddCharacterToWorld(const Vector3& position);
			GameObject* AddAppleToWorld(const Vector3& position, int appleIndex);
			GameObject* AddSoilToWorld(const Vector3& position, Vector3 dimensions, float inverseMass);
			GameObject* AddParkKeeperToWorld(EnemyObject* keeper);
			GameObject* AddSpawnPoint(const Vector3& position, Vector3 dimensions, float inverseMass);

			Vector2 WorldPosToNodePos(float x, float y);

			GameTechRenderer*	renderer;
			PhysicsSystem*		physics;
			GameWorld*			world;

			bool useGravity;
			bool inSelectionMode;

			float		forceMagnitude;

			GameObject* selectionObject = nullptr;

			OGLMesh*	cubeMesh	= nullptr;
			OGLMesh*	sphereMesh	= nullptr;
			OGLTexture* basicTex	= nullptr;
			OGLShader*	basicShader = nullptr;

			//Coursework Meshes
			OGLMesh*	gooseMesh	= nullptr;
			OGLMesh*	keeperMesh	= nullptr;
			OGLMesh*	appleMesh	= nullptr;
			OGLMesh*	charA		= nullptr;
			OGLMesh*	charB		= nullptr;

			//Coursework Additional functionality	
			GameObject* lockedObject	= nullptr;
			Vector3 lockedOffset		= Vector3(0, 14, 20);
			void LockCameraToObject(GameObject* o) 
			{
				lockedObject = o;
			}
			MazeData* gameMap;

			GoosePlayer* playerObject = nullptr;
			std::vector<EnemyObject*> enemies;
			AppleObject* apples[NUMBER_OF_APPLES];

			DestinationObject* destObject;

			std::vector<Node> vacantRoads;

			GameTimer timer;

			std::vector<PositionConstraint*> constraintList;
			void UpdateConstraint(float dt);
			PushdownMachine* pushdownStateMachine = new PushdownMachine();
			PushdownState* gameState = new PushdownState([&](PushdownState* sender, float dt)
				{
					if (Window::GetKeyboard()->KeyPressed(KeyboardKeys::C))
					{
						pushdownStateMachine->PushState(cheatingMode);
					}
					Debug::Print("Press C to enter cheating mode!", Vector2(0, 110), Vector4(1, 1, 1, 1));
					GameStateUpdate(sender, dt);
				});
				float injuryTime = 0;
				float pauseTik = 0;
			PushdownState* pauseState = new PushdownState([&](PushdownState* sender, float dt) 
				{
					Debug::Print("Game Paused! Press R to resume!", Vector2(0, 20), Vector4(1, 1, 1, 1));
					if (Window::GetKeyboard()->KeyPressed(KeyboardKeys::R))
					{
						pushdownStateMachine->PopState();
					}
				},
				[&](PushdownState* sender)
				{
					pauseTik = timer.GetTotalTimeSeconds();
				},
				[&](PushdownState* sender)
				{
					pauseTik = timer.GetTotalTimeSeconds() - pauseTik;
					injuryTime += pauseTik;
				});
			PushdownState* cheatingMode = new PushdownState([&](PushdownState* sender, float dt)
				{
					if (Window::GetKeyboard()->KeyPressed(KeyboardKeys::T))
					{
						playerObject->GetTransform().SetWorldPosition(playerObject->initPos);
					}
					if(Window::GetKeyboard()->KeyPressed(KeyboardKeys::V))
					{
						pushdownStateMachine->PopState();
					}
					Debug::Print("Press V to exit cheating mode!", Vector2(0, 110), Vector4(1, 1, 1, 1));
					Debug::Print("Press T to teleport to destination!", Vector2(0, 130), Vector4(1, 1, 1, 1));
					GameStateUpdate(sender, dt);
				});
			std::function<void(PushdownState*, float)> GameStateUpdate =
				[&](PushdownState* sender, float deltaTime)
			{
				if (Window::GetKeyboard()->KeyPressed(KeyboardKeys::P) &&
					!destObject->GetGameOver())
				{
					pushdownStateMachine->PushState(pauseState);
				}
				if (Window::GetKeyboard()->KeyPressed(KeyboardKeys::F1))
				{
					injuryTime = 0;
					timer = GameTimer();
					playerObject->ResetPlayer();
					destObject->ResetGame();
					for (auto* item : enemies)
					{
						item->GetTransform().SetWorldPosition(item->initPos);
					}
					for (auto* item : apples)
					{
						item->Reset();
					}
				}
				float currentGameTime = max(GAME_TIME - timer.GetTotalTimeSeconds() + injuryTime, 0);
				if (currentGameTime <= 0 || destObject->GetNumberOfApples() >= NUMBER_OF_APPLES)
				{
					currentGameTime = 0;
					destObject->SetGameOver();
					client->SendPacket(HighScorePacket(destObject->GetTotalPoints()));
				}
				Debug::Print("Press P to pause!", Vector2(0, 90), Vector4(1, 1, 1, 1));
				std::stringstream ss;
				if (destObject->GetGameOver())
				{
					ss << "GAMEOVER!!! Final ";
				}
				ss << "Score: " << destObject->GetTotalPoints() << " Countdown: " << (int)currentGameTime;
				Debug::Print(ss.str(), Vector2(0, 70), Vector4(0, 0, 0, 1));
				ss.str("");
				ss << "Collected Apples: " << destObject->GetNumberOfApples() << "/" << NUMBER_OF_APPLES;
				Debug::Print(ss.str(), Vector2(0, 50), Vector4(0, 0, 0, 1));
				Debug::Print("Press F1 anytime to restart.", Vector2(0, 30), Vector4(0, 0, 0, 1));
				for (auto* item : apples)
				{
					item->UpdateStateMachine();
				}
				if (!destObject->GetGameOver())
				{
					for (auto* item : enemies)
					{
						item->UpdateStateMachine();
						if (item->GetChasingPath().size() < 2)
						{
							continue;
						}
						for (int i = 0; i < item->GetChasingPath().size() - 1; i++)
						{
							Debug::DrawLine(item->GetChasingPath()[i], item->GetChasingPath()[i + 1], Vector4(1, 0, 0, 1));
						}
						Vector3 from = item->GetTransform().GetWorldPosition();
						Vector3 to = item->GetChasingPath()[item->GetChasingPath().size() - 2];
						float speed = 5 - (item->IsInWater() ? 2 : 0);
						Vector3 delta = (to - from).Normalized();
						item->GetTransform().SetWorldPosition(from + delta * speed * deltaTime);

						Vector3 z = item->GetTransform().GetWorldPosition() - to;
						z.Normalize();
						z = -z; // Force each enemy to look at opposite direction to solve model problem.
						Vector3 x = Vector3::Cross(Vector3(0, 1, 0), z);
						Vector3 y = Vector3::Cross(z, x);

						Matrix4 rotation;

						rotation.array[0] = x.x;
						rotation.array[1] = x.y;
						rotation.array[2] = x.z;

						rotation.array[4] = y.x;
						rotation.array[5] = y.y;
						rotation.array[6] = y.z;

						rotation.array[8] = z.x;
						rotation.array[9] = z.y;
						rotation.array[10] = z.z;

						rotation.array[15] = 1;

						item->GetTransform().SetLocalOrientation(rotation);
					}
				}
				if (!inSelectionMode)
				{
					world->GetMainCamera()->CameraFollow(playerObject->GetTransform().GetWorldPosition(), 7, 15, playerObject->GetTransform().GetWorldOrientation());

					Vector3 playerPosition = playerObject->GetTransform().GetWorldPosition();
					float force = 25;
					force -= playerObject->GetNumberOfPickedApples() * 3;
					if (playerObject->IsInWater())
					{
						force = 5;
					}
					force = max(force, 5);
					if (Window::GetKeyboard()->KeyDown(KeyboardKeys::SHIFT) && !playerObject->IsInWater())
					{
						force *= 2;
					}
					if (Window::GetKeyboard()->KeyDown(KeyboardKeys::W))
					{
						Vector3 forward = playerObject->GetTransform().GetWorldOrientation() * Vector3(0, 0, 1);
						forward.Normalize();
						playerObject->GetPhysicsObject()->AddForce(forward * force);
					}
					if (Window::GetKeyboard()->KeyDown(KeyboardKeys::S))
					{
						Vector3 forward = playerObject->GetTransform().GetWorldOrientation() * Vector3(0, 0, 1);
						forward.Normalize();
						playerObject->GetPhysicsObject()->AddForce(-forward * force);
					}
					Quaternion playerOrientation = playerObject->GetTransform().GetLocalOrientation();
					if (Window::GetKeyboard()->KeyDown(KeyboardKeys::A))
					{
						playerObject->GetPhysicsObject()->AddTorque(Vector3(0, 1, 0));
					}
					if (Window::GetKeyboard()->KeyDown(KeyboardKeys::D))
					{
						playerObject->GetPhysicsObject()->AddTorque(Vector3(0, -1, 0));
					}
					playerObject->GetTransform().SetLocalOrientation(Quaternion::EulerAnglesToQuaternion(0, playerOrientation.ToEuler().y, 0));
				}
				else
				{
					world->GetMainCamera()->UpdateCamera(deltaTime);
				}


				SelectObject();
				MoveSelectedObject();

				UpdateConstraint(deltaTime);
				world->UpdateWorld(deltaTime);
				renderer->Update(deltaTime);
				physics->Update(deltaTime);
			};
			const bool isServer;
			std::set<int> highScoreQueue;
		};
	}
}

