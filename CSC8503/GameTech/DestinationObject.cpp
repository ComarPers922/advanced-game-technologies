#include "DestinationObject.h"
#include "AppleObject.h"

void NCL::CSC8503::DestinationObject::AddApples(std::vector<AppleObject*>& apples)
{
		int accPoints = 0;
		int accNum = 1;
		for (auto* item : apples)
		{
			applesAtDest.push_back(item);
			accPoints += item->Points * (accNum++);
			item->IsAtDestination();
		}
		totalPoints += gameover ? 0 :  accPoints;
		apples.clear();
}