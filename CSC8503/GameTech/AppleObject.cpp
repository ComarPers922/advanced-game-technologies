#include "AppleObject.h"
#include "GoosePlayer.h"

NCL::CSC8503::AppleObject::AppleObject(GameObject* player, 
	Vector3 origin, 
	DestinationObject* destObj, 
	int points, 
	std::function<void(AppleObject*)> pickedAppleDelegate) :
	destObject(destObj),
	playerObject(player),
	originalPoint(origin),
	appleStateMachine(new StateMachine()),
	idleFunc
	([](void* data)
		{
			auto* apple = (AppleObject*)data;
			apple->GetTransform().SetWorldPosition(apple->originalPoint);
			apple->isReset = false;
		}),
	pickedFunc([](void* data)
		{
			auto* apple = (AppleObject*)data;
			// apple->GetTransform().SetWorldPosition(apple->playerObject->GetTransform().GetWorldPosition() + Vector3(0, 1 + apple->offsetYGoose, 0));
		}),
	destFunc([](void* data)
		{
			auto* apple = (AppleObject*)data;
			apple->GetTransform().SetWorldPosition(apple->destObject->GetTransform().GetWorldPosition() + Vector3(0, 1 + apple->offsetYDest, 0));
		}),
		idleState(new GenericState(idleFunc, this)),
		pickedState(new GenericState(pickedFunc, this)),
		destState(new GenericState(destFunc, this)),
		idleToPicked(new GenericTransition<bool&, bool>(GenericTransition<bool&, bool>::EqualsTransition, isAttatchedToPlayer, true, idleState, pickedState)),
		pickedToIdle(new GenericTransition<bool&, bool>(GenericTransition<bool&, bool>::EqualsTransition, isAttatchedToPlayer, false, pickedState, idleState)),
		pickedToDest(new GenericTransition<bool&, bool>(GenericTransition<bool&, bool>::EqualsTransition, isAtDest, true, pickedState, destState)),
		destToIdle(new GenericTransition<bool&, bool>(GenericTransition<bool&, bool>::EqualsTransition, isReset, true, destState, idleState)),
		pickedAppleDelegate(pickedAppleDelegate),
		Points(points)
{
	appleStateMachine->AddState(idleState);
	appleStateMachine->AddState(pickedState);
	appleStateMachine->AddState(destState);

	appleStateMachine->AddTransition(idleToPicked);
	appleStateMachine->AddTransition(pickedToIdle);
	appleStateMachine->AddTransition(pickedToDest);
	appleStateMachine->AddTransition(destToIdle);
}

NCL::CSC8503::AppleObject::~AppleObject()
{
	delete appleStateMachine;
	delete idleState;
	delete pickedState;
}

void NCL::CSC8503::AppleObject::OnCollisionBegin(GameObject* otherObject)
{
	if (otherObject->GetPhysicsObject()->GetPhysicsLayer() == Layer::Player && !isAttatchedToPlayer)
	{
		GoosePlayer* player = (GoosePlayer*)otherObject;
		player->addApple(this);
		offsetYGoose = player->GetNumberOfPickedApples();
		isAttatchedToPlayer = true;
		pickedAppleDelegate(this);
	}
}

void NCL::CSC8503::AppleObject::UpdateStateMachine()
{
	appleStateMachine->Update();
}
