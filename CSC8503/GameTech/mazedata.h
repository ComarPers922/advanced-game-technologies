#pragma once
#include "randomqueue.h"
#include "position.h"
#include <ctime>
#include <cmath>
#include <cstdlib>

#include <unordered_set>
#include <queue>
#include <memory>
#include "../../Common/Vector2.h"

using std::unordered_set;
using std::priority_queue;

constexpr auto ROAD = 'r';
constexpr auto WALL = 'w';
constexpr auto MAP_SIZE = 43;

constexpr int DIRECTIONS[4][2] = { {-1,0},{0,1},{1,0},{0,-1} };
struct Node
{
	bool walkable = false;
	int x = 0;
	int y = 0;
	
	int gCost = 0;
	int hCost = 0;

	Node* from = nullptr;

	Node(const int& x, const int& y)
	{
		this->x = x;
		this->y = y;
	}

	bool operator< (const Node& n1)
	{
		return (gCost + hCost) < (n1.gCost + n1.hCost);
	}

	bool operator> (const Node& n1)
	{
		return (gCost + hCost) > (n1.gCost + n1.hCost);
	}

	bool operator==(const Node& n1)
	{
		return gCost == n1.gCost &&
			hCost == n1.hCost;
	}
};	



class NodeHashFunction
{
public:
	size_t operator()(const Node& n) const
	{
		return n.x * n.y * 7919;
	}
};

class MazeData
{
public:
    MazeData();

    char maze[MAP_SIZE][MAP_SIZE];
    bool visited[MAP_SIZE][MAP_SIZE];
    bool inArea(int x, int y);
    void reset();
	void Generate();

	int* GetValidNeighbor(int x, int y);
	int* GetValidNeighbor(const NCL::Maths::Vector2& pos);

	Node* FindPath(int from[2], int to[2]);

    static const int height = MAP_SIZE;

    const int entranceX = 0;
    const int entranceY = 1;
    const int exitX = MAP_SIZE - 1;
    const int exitY = MAP_SIZE - 2;
};
