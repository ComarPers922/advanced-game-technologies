#pragma once
#include "..\CSC8503Common\GameObject.h"
#include <vector>

namespace NCL
{
	namespace CSC8503
	{
		class AppleObject;

		class DestinationObject :
			public GameObject
		{
		public:
			int GetNumberOfApples()
			{
				return applesAtDest.size();
			}
			int GetTotalPoints()
			{
				return totalPoints;
			}
			void SetGameOver()
			{
				gameover = true;
			}
			bool GetGameOver()
			{
				return gameover;
			}
			void ResetGame()
			{
				gameover = false;
				totalPoints = 0;
				applesAtDest.clear();
			}
			void AddApples(std::vector<AppleObject*>& apples);
		private:
			std::vector<AppleObject*> applesAtDest;
			int totalPoints = 0;
			bool gameover = false;
		};
	}
}

