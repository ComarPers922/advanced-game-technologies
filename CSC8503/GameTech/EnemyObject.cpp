#include "EnemyObject.h"
#include "TutorialGame.h"

NCL::CSC8503::EnemyObject::EnemyObject(GoosePlayer* player, MazeData* map, Vector3 initPos) :
	initPos(initPos),
	playerObject(player),
	enemyStateMachine(new StateMachine()),
	gameMap(map),
	chasingFunc([](void* data)
		{
			EnemyObject* enemyData = (EnemyObject*)data;

			enemyData->chasingPath.clear();
			Vector3 enemyPos = enemyData->GetTransform().GetWorldPosition() + WORLD_ORIGIN_POINT;
			Vector2 fromNode = Vector2((int)(enemyPos.x / 2 + 0.5), (int)(enemyPos.z / 2 + 0.5));
			int* from= enemyData->gameMap->GetValidNeighbor(fromNode);

			Vector3 toPos = enemyData->playerObject->GetTransform().GetWorldPosition() + WORLD_ORIGIN_POINT;
			int* to = enemyData->gameMap->GetValidNeighbor(Vector2((int)(toPos.x / 2 + 0.5), (int)(toPos.z / 2 + 0.5)));
			Node* path = enemyData->gameMap->FindPath(from, to);
			delete[] to;
			delete[] from;
			auto* currentNode = path;
			while (currentNode != nullptr)
			{
				enemyData->chasingPath.push_back(Vector3(2 * currentNode->x, enemyData->GetTransform().GetWorldPosition().y, 2 * currentNode->y) - WORLD_ORIGIN_POINT);
				auto* tempNode = currentNode;
				currentNode = currentNode->from;
				delete tempNode;
			}
		}),
	idleFunc([](void* data)
		{
			EnemyObject* enemyData = (EnemyObject*)data;
			enemyData->chasingPath.clear();
		}),
		idleState(new GenericState(idleFunc, this)),
		chasingState(new GenericState(chasingFunc, this)), 
		chasingToIdle(new GenericTransition<bool&, bool>(GenericTransition<bool&, bool>::EqualsTransition, shouldChase, false, chasingState, idleState)),
		idleToChasing(new GenericTransition<bool&, bool>(GenericTransition<bool&, bool>::EqualsTransition, shouldChase, true, idleState, chasingState))
{
	enemyStateMachine->AddState(idleState);
	enemyStateMachine->AddState(chasingState);

	enemyStateMachine->AddTransition(idleToChasing);
	enemyStateMachine->AddTransition(chasingToIdle);
}

NCL::CSC8503::EnemyObject::~EnemyObject()
{
	delete enemyStateMachine;

	delete idleState;
	delete chasingState;

	delete idleToChasing;
	delete chasingToIdle;
}
